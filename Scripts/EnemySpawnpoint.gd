extends Position2D

onready var EnemySpawnTimer = $EnemySpawnTimer
onready var Player = get_parent().get_parent().get_node("Player")
onready var EnemyOrganizer = get_parent().get_parent().get_node("EnemyOrganizer")


var enemy_dictionary = {
	"Carrier": preload("res://Objects/Enemies/Carrier.tscn"),
	"Disk": preload("res://Objects/Enemies/Disk.tscn"),
	"Scout": preload("res://Objects/Enemies/Scout.tscn"),
	"Monolith": preload("res://Objects/Enemies/Monolith.tscn"),
	"Shell": preload("res://Objects/Enemies/Shell.tscn")
 }

var spawnable_enemies = [enemy_dictionary["Monolith"],
enemy_dictionary["Carrier"], 
enemy_dictionary["Disk"], enemy_dictionary["Scout"],]


#func on_powerup_unlocked(powerup_name):
#	match powerup_name:
#		"Netzach":
#			spawnable_enemies.append(enemy_dictionary["Carrier"])
#			EnemySpawnTimer.start()


func spawn_random_enemy():
	var random_enemy = spawnable_enemies[randi() % spawnable_enemies.size()]
	var random_enemy_instance = random_enemy.instance()
	random_enemy_instance.position = self.position
	EnemyOrganizer.add_child(random_enemy_instance)
	ScoreManager.enemies_on_level += 1


func _on_EnemySpawnTimer_timeout() -> void:
	if ScoreManager.enemies_on_level <= ScoreManager.enemy_limit:
		spawn_random_enemy()


