extends "res://Scripts/Inimigo.gd"

export (Dictionary) var DIRECTIONS = {
	"NORTH": Vector2(0,-1),
	"SOUTH": Vector2(0,1),
	"EAST" : Vector2(1,0),
	"WEST" : Vector2(-1,0)
	}

var player

func _behaviour():
	player = get_player()
	go_to_player()	
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.has_method("get_hit_by_enemy"):
			collision.collider.get_hit_by_enemy()
			self_destruct()
			queue_free()
	

func go_to_player():
	move_and_slide(position.direction_to(player.position).normalized() * SPEED)
	look_at(player.global_position)
	rotate(deg2rad(-90))


func self_destruct():
	for dir in DIRECTIONS:
		shoot()
		


func get_hit_by_shot():
	life -= 1
	print(life)
	if life <= 0:
		self_destruct()
		queue_free()
