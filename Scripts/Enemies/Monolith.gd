extends "res://Scripts/Inimigo.gd"

onready var player = get_parent().get_parent().get_node("Player")
onready var MonolithTween = $MonolithTween

var rotation_to_player


func _ready() -> void:
	choose_new_direction()


func reflect_shot(shot_position, shot_direction):
	var reflected_shot = enemy_shot_scene.instance()
	reflected_shot.position = shot_position
	reflected_shot.direction = -shot_direction
	EnemyShots.add_child(reflected_shot)


func _behaviour():
	move()
	
	


func _on_TurnTimer_timeout() -> void:
	set_look_rotation()


func _on_MoveTimer_timeout() -> void:
	choose_new_direction()

func set_look_rotation():
	rotation_to_player = position.angle_to_point(player.position) + deg2rad(270)
	MonolithTween.interpolate_property(self, "rotation", rotation, rotation_to_player, 0.5)
	MonolithTween.start()
	
