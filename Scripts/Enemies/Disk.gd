extends "res://Scripts/Inimigo.gd"

onready var ShootTimer = $ShootTimer
onready var MoveTimer = $MoveTimer
onready var DiskTween = $DiskTween

func _ready() -> void:
	ShotSpawns = $ShotSpawns
	DiskTween.interpolate_property(self, "rotation_degrees", 0, 360, 4)
	DiskTween.start()
	MoveTimer.start()

func _behaviour():
	move()


func _on_MoveTimer_timeout() -> void:
	choose_new_direction()


func _on_ShootTimer_timeout() -> void:
	shoot()
