extends "res://Scripts/Inimigo.gd"


var player

func _behaviour():
	player = get_player()
	go_to_player()	
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.has_method("get_hit_by_enemy"):
			collision.collider.get_hit_by_enemy()
			queue_free()
		

	
	

func go_to_player():
	move_and_slide(position.direction_to(player.position).normalized() * SPEED)
	look_at(player.global_position)
	rotate(deg2rad(-90))
	


