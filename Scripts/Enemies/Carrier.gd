extends "res://Scripts/Inimigo.gd"

var direction
onready var CarrierTween = $CarrierTween


func _ready() -> void:
	ShotSpawns = $ShotSpawns
	choose_new_direction()


func _behaviour():
		move()

func _on_ShootTimer_timeout() -> void:
	shoot()


func _on_MoveTimer_timeout() -> void:
	choose_new_direction()
	choose_new_rotation()
	var current_rotation = rotation_degrees
	CarrierTween.interpolate_property(self, "rotation_degrees", current_rotation, new_rotation, 2)
	CarrierTween.start()
