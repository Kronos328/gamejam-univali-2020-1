extends Control

func _ready() -> void:
	get_tree().paused = true


func _input(event: InputEvent) -> void:
	if Input.is_action_pressed("Pause_Unpause"):
		get_tree().paused = false
		queue_free()


func _on_BackToMenu_pressed() -> void:
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
	get_tree().paused = false
	queue_free()
