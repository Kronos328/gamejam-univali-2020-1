extends Control


func _on_QuitGame_pressed() -> void:
	get_tree().quit()


func _on_Play_pressed() -> void:
	var preplay_instance = load("res://Scenes/UI Windows/PreplayScreen.tscn")
	preplay_instance = preplay_instance.instance()
	get_parent().add_child(preplay_instance)


func _on_Credits_pressed() -> void:
	var credits_instance = load("res://Scenes/UI Windows/CreditsScreen.tscn")
	credits_instance = credits_instance.instance()
	get_parent().add_child(credits_instance)
