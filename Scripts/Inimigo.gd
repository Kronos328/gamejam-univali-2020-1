extends KinematicBody2D

onready var StageNav = get_parent().get_parent().get_node("StageNav")
var enemy_shot_scene = preload("res://Objects/EnemyShot.tscn")
var explosion = preload("res://Objects/Explosion.tscn")
export (int) var life
export (int) var SPEED
var new_direction = Vector2()
var new_rotation
var velocity = Vector2()
var ShotSpawns
var SFXPlayer: AudioStreamPlayer2D
onready var EnemyShots = $"../../EnemyShotsOrganizer"


# warning-ignore:unused_argument
func _physics_process(delta: float) -> void:
	_behaviour()

func _behaviour():
	pass


func get_player():
	var player = get_parent().get_parent().get_node("Player")
	return player


func shoot():
	for shot_spawn in ShotSpawns.get_children():
		EnemyShots.grab_bullet(shot_spawn)


func get_hit_by_shot():
	life -= 1
	print(life)
	if life <= 0:
		queue_free()
		ScoreManager.add_score(1)
		ScoreManager.update_score_call()
		ScoreManager.enemies_on_level -= 1
		instance_explosion()
		


func move():
	velocity = new_direction * SPEED
	velocity = move_and_slide(velocity)


func choose_new_direction():
	new_direction = Vector2(rand_range(-1,1),rand_range(-1,1))


func choose_new_rotation():
	new_rotation = rand_range(0,360)


func self_destroy():
	queue_free()

func instance_explosion():
	var explosion_instance = explosion.instance()
	explosion_instance.global_position = self.global_position
	get_parent().get_parent().add_child(explosion_instance)
	
