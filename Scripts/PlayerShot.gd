extends KinematicBody2D

export(int) var SPEED
var direction = Vector2(1,1)
 


func _physics_process(delta: float) -> void:
	var collision = move_and_collide((direction * SPEED) * delta)
	if collision:
		if collision.collider_shape.is_in_group("Reflect"):
			collision.collider.reflect_shot(self.position, self.direction)
		elif collision.collider.has_method("get_hit_by_shot"):
			collision.collider.get_hit_by_shot()
		queue_free()


