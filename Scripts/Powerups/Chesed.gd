extends "res://Scripts/Powerups/Powerup.gd"

var chesed_area_scene = preload("res://Objects/Powerups/ChesedArea.tscn")
onready var Player = get_parent().get_parent()


func _ready() -> void:
	CooldownTimer = $ChesedCooldownTimer


func powerup_effect():
	is_on_cooldown = true
	CooldownTimer.start()
	var chesed_area_instance = chesed_area_scene.instance()
	chesed_area_instance.global_position = Player.global_position
	Player.get_parent().add_child(chesed_area_instance)


func _on_ChesedCooldownTimer_timeout() -> void:
	is_on_cooldown = false
