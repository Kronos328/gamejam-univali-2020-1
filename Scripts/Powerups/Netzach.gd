extends "res://Scripts/Powerups/Powerup.gd"

onready var NetzachParticles = $NetzachParticles
var netzach_cloud_scene = preload("res://Objects/Powerups/NetzachSmokeCloud.tscn")
var Player
export (int) var teleport_distance
var position_after_teleport = Vector2()
var max_distances = { "Up": -537, "Down": 5223, "Left": -962, "Right": 2278 }



func _ready() -> void:
	Player = get_player()
	CooldownTimer = $NetzachCooldownTimer


func powerup_effect():
	is_on_cooldown = true
	CooldownTimer.start()
	var smoke_instance = netzach_cloud_scene.instance()
	smoke_instance.global_position = Player.global_position
	get_parent().get_parent().get_parent().add_child(smoke_instance)
	NetzachParticles.emitting = true
	teleport(teleport_distance)
	check_if_out_of_bounds()
	


func check_if_out_of_bounds():
	if Player.position.y < max_distances["Up"]:
		Player.position.y = max_distances["Up"] + 64
	if Player.position.y > max_distances["Down"]:
		Player.position.y = max_distances["Down"] - 64
	if Player.position.x < max_distances["Left"]:
		Player.position.x = max_distances["Left"] + 64
	if Player.position.x > max_distances["Right"]:
		Player.position.x = max_distances["Right"] - 64
	


func teleport(tel_distance):
	Player.position += Vector2(Player.horizontal_movement, Player.vertical_movement) * tel_distance


func _on_NetzachCooldownTimer_timeout() -> void:
	is_on_cooldown = false
