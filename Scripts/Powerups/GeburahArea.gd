extends Area2D

onready var GeburahAreaSprite = $GeburahAreaSprite
onready var GeburahAreaDurationTimer = $GeburahAreaDurationTime

func _physics_process(delta: float) -> void:
	for body in get_overlapping_bodies():
		if body.has_method("self_destroy"):
			body.self_destroy()

func _ready() -> void:
	GeburahAreaSprite.play("InitialBlast")

func _on_GeburahAreaSprite_animation_finished() -> void:
	GeburahAreaSprite.play("SustainedBlast")


func _on_GeburahAreaDurationTime_timeout() -> void:
	queue_free()
