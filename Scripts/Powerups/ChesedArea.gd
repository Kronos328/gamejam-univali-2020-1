extends Area2D

onready var ChesedAreaTween = $ChesedAreaTween
onready var ChesedAreaAnim = $ChesedAreaAnim

export (int) var life

func _ready() -> void:
	ChesedAreaAnim.play("InitialAnim")


#func _physics_process(delta: float) -> void:
#	for body in get_overlapping_bodies():
#		if body.is_in_group("EnemyShot"):
#			reduce_life()
#			body.queue_free()


func _on_ChesedAreaAnim_animation_finished() -> void:
	ChesedAreaAnim.play("SustainedAnim")
	rotate_sprite()


func reduce_life():
	life -= 1
	if life <= 0:
		queue_free()


func rotate_sprite():
	ChesedAreaTween.interpolate_property(ChesedAreaAnim, "rotation_degrees", 0, 360, 4)
	ChesedAreaTween.start()


func _on_ChesedAreaTween_tween_completed(object: Object, key: NodePath) -> void:
	rotate_sprite()

func get_hit_by_enemy():
	reduce_life()
