extends "res://Scripts/Powerups/Powerup.gd"

onready var DurationTimer = $HodDurationTimer
onready var HodParticles = $HodParticles
onready var HodAnimation = $HodAnimation

var player

func _ready() -> void:
	player = get_player()
	CooldownTimer = $HodCooldownTimer

func powerup_effect():
	player.shielded = true
	DurationTimer.start()
	HodParticles.emitting = true
	HodAnimation.visible = true
	HodAnimation.play("ShieldUp")


func _on_HodDurationTimer_timeout() -> void:
	HodParticles.emitting = false
	HodAnimation.stop()
	HodAnimation.visible = false
	player.shielded = false
	
