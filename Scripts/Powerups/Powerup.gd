extends Sprite

export (bool) var unlocked = false
var is_on_cooldown = false
var CooldownTimer: Timer

signal unlocked_powerup(powerup_name)

func _ready() -> void:
	pass 

func _process(_delta: float) -> void:
	match unlocked:
		true:
			return
		false:
			self.visible = false


func powerup_effect():
	pass


func unlock():
	unlocked = true
	visible = true
	emit_signal("unlocked_powerup", name)


func get_player():
	var player = get_parent().get_parent()
	return player


func enter_cooldown_time():
	CooldownTimer.start()
