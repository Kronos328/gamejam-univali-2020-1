extends "res://Scripts/Powerups/Powerup.gd"

onready var EnemyShots = $"../../../EnemyShotsOrganizer"
onready var player = get_parent().get_parent()
onready var PowerupTween = $"../PowerupTween"
onready var ChokmahParticles = $ChokmahParticles
onready var ChokmahAnimationSprite = $ChokmahAnimationSprite2

export (Vector2) var initial_scale

func _ready() -> void:
	CooldownTimer = $ChokmahCooldownTimer

func powerup_effect():
	animate_circle()
	is_on_cooldown = true
	CooldownTimer.start()
	for shot in EnemyShots.get_children():
		if shot.EnemyShotVN.is_on_screen():
			var player_shot_instance = player.shot.instance()
			player_shot_instance.global_position = shot.global_position
			player_shot_instance.direction = shot.direction
			EnemyShots.bullet_destroy(shot)
			player.get_parent().add_child(player_shot_instance)


func _on_ChokmahCooldownTimer_timeout() -> void:
	is_on_cooldown = false


func animate_circle():
	ChokmahParticles.emitting = true
	ChokmahAnimationSprite.visible = true
	PowerupTween.interpolate_property(ChokmahAnimationSprite, "scale", initial_scale, Vector2(3,3), 0.75)
	PowerupTween.start()


func _on_PowerupTween_tween_completed(object: Object, key: NodePath) -> void:
	if object == ChokmahAnimationSprite:
		ChokmahAnimationSprite.visible = false
