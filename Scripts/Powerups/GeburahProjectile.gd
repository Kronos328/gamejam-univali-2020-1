extends KinematicBody2D

onready var GeburahProjectileTween = $GeburahProjectileTween
onready var GeburahProjectileSprite = $GeburahProjectileSprite
onready var GeburahProjectileExplodeTimer = $GeburahProjectileExplodeTimer

var geburah_area_scene = preload("res://Objects/Powerups/GeburahArea.tscn")

export (int) var SPEED 
var direction = Vector2(1,1)

func _physics_process(delta: float) -> void:
	var collision = move_and_collide((direction * SPEED) * delta)
	if collision:
		_on_GeburahProjectileExplodeTimer_timeout()

func _ready() -> void:	
	rotate_sprite()

func rotate_sprite():
	GeburahProjectileTween.interpolate_property(GeburahProjectileSprite, "rotation_degrees", 0, 360, 1)
	GeburahProjectileTween.start()


func _on_GeburahProjectileTween_tween_completed(object: Object, key: NodePath) -> void:
	rotate_sprite()


func _on_GeburahProjectileExplodeTimer_timeout() -> void:
	instance_area()
	queue_free()


func instance_area():
	var area_instance = geburah_area_scene.instance()
	area_instance.global_position = global_position
	get_parent().add_child(area_instance)
