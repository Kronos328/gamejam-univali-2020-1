extends "res://Scripts/Powerups/Powerup.gd"

onready var ShotSpot = $"../../ShotSpot"
onready var Player = get_parent().get_parent()

var geburah_projectile_scene = preload("res://Objects/Powerups/GeburahProjectile.tscn")

func _ready() -> void:
	CooldownTimer = $GeburahCooldownTime

func powerup_effect():
	is_on_cooldown = true
	CooldownTimer.start()
	var instanced_projectile = geburah_projectile_scene.instance()
	instanced_projectile.global_position = ShotSpot.global_position
	instanced_projectile.direction = instanced_projectile.direction.rotated(Player.rotation + deg2rad(45))
	Player.get_parent().add_child(instanced_projectile)


func _on_GeburahCooldownTime_timeout() -> void:
	is_on_cooldown = false
