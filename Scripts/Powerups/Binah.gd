extends "res://Scripts/Powerups/Powerup.gd"

onready var EnemyShots = get_parent().get_parent().get_parent().get_node("EnemyShotsOrganizer")
onready var PowerupTween = $"../PowerupTween"
onready var BinahAnimationSprite = $BinahAnimationSprite
onready var BinahParticles = $BinahParticles


export (Vector2) var initial_scale

func _ready() -> void:
	CooldownTimer = $BinahCooldownTimer

func _process(delta: float) -> void:
	BinahParticles.global_rotation_degrees = 0

func powerup_effect():
	is_on_cooldown = true
	CooldownTimer.start()
	animate_circle()
	for shot in EnemyShots.get_children():
		if shot.EnemyShotVN.is_on_screen():
				EnemyShots.bullet_destroy(shot)


func animate_circle():
	BinahParticles.emitting = true
	BinahAnimationSprite.visible = true
	PowerupTween.interpolate_property(BinahAnimationSprite, "scale", initial_scale, Vector2(3,3), 0.75)
	PowerupTween.start()


func _on_PowerupTween_tween_completed(object: Object, key: NodePath) -> void:
	if object == BinahAnimationSprite:
		BinahAnimationSprite.visible = false


func _on_BinahCooldownTime_timeout() -> void:
	is_on_cooldown = false
