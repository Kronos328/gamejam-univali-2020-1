extends Control

onready var PontuacaoText = $PontuacaoText


func _ready() -> void:
	PontuacaoText.text = "Sua pontuação foi: " + str(ScoreManager.score)

func _on_Button_pressed() -> void:
	SilentWolf.Scores.persist_score($LineEdit.text, ScoreManager.score, "TheLastGear")
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
