extends Control

onready var player = $"../../Player"
onready var LabelVida = $PowerupAndLifeRect/VidasLabel
onready var LabelPowerup = $PowerupAndLifeRect/TextoPowerupAtual
onready var LabelScore = $ScoreRect/ScoreLabel
onready var CurrentPowerupImage = $PowerupAndLifeRect/ImagemPowerupAtual
var powerup_text_name
var powerup_image
var current_wave

func _ready() -> void:
	ScoreManager.UI = self

func update_current_powerup():
	match player.current_powerup:
		player.POWERUPS.NETZACH:
			powerup_text_name = "Netzach"
			powerup_image = "res://Sprites/Powerups/sefirot verdel.png"
		player.POWERUPS.BINAH:
			powerup_text_name = "Binah"
			powerup_image = "res://Sprites/Powerups/sefirot pretal.png"
		player.POWERUPS.HOD:
			powerup_text_name = "Hod"
			powerup_image = "res://Sprites/Powerups/sefirot laranja.png"
		player.POWERUPS.GEBURAH:
			powerup_text_name = "Geburah"
			powerup_image = "res://Sprites/Powerups/sefirot vermelha.png"
		player.POWERUPS.CHESED:
			powerup_text_name = "Chesed"
			powerup_image = "res://Sprites/Powerups/sefirot azul.png"
		player.POWERUPS.CHOKMAH:
			powerup_text_name = "Chokmah"
			powerup_image = "res://Sprites/Powerups/sefirot cinzal.png"
	LabelPowerup.text = powerup_text_name
	CurrentPowerupImage.texture = load(powerup_image)

func update_life():
	LabelVida.text = "Vida: " + str(player.life)

func update_score(score):
	LabelScore.text = "Score: " + str(score)


