extends KinematicBody2D

onready var ShotSpot = $ShotSpot
onready var UI = $"../UILayer/UI"
onready var ShotSFXPlayer = $ShotSFXPlayer

export(int) var SPEED
export (int) var life

var shot = preload("res://Objects/PlayerShot.tscn")
var horizontal_movement
var vertical_movement
var velocity = Vector2()
var face_direction
enum POWERUPS {HOD, BINAH, NETZACH, GEBURAH, CHOKMAH, CHESED}
export (POWERUPS) var current_powerup
var shielded  = false
var powerup_index = 0



func _ready() -> void:
	ScoreManager.setup_new_game_score()
	assign_current_powerup()
	UI.update_current_powerup()
	UI.update_life()


func _physics_process(delta: float) -> void:
	get_move_input()
	move_character()
	face_mouse()	


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("shoot"):
		shoot()
	if event.is_action_pressed("use_current_powerup"):
		use_powerup()
	if Input.is_action_pressed("next_powerup"):
		next_powerup()
		UI.update_current_powerup()
	if Input.is_action_pressed("previous_powerup"):
		previous_powerup()
		UI.update_current_powerup()
	if Input.is_action_pressed("Pause_Unpause"):
		var pause_screen = load("res://Scenes/UI Windows/PauseScreen.tscn")
		pause_screen = pause_screen.instance()
		UI.get_parent().add_child(pause_screen)


func get_move_input():
	horizontal_movement = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	vertical_movement = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	

func move_character():
	velocity = Vector2(horizontal_movement,vertical_movement) * SPEED
	move_and_slide(velocity) 


func face_mouse():
	look_at(get_global_mouse_position())
	rotate(deg2rad(-90))


func shoot():
	var shot_instance = shot.instance()
	var direction = Vector2()
	shot_instance.position = ShotSpot.global_position
	shot_instance.direction  = shot_instance.direction.rotated(rotation+deg2rad(45))
	print(shot_instance.position)
	get_tree().get_root().add_child(shot_instance)
	ShotSFXPlayer.play()


func use_powerup():
	match current_powerup:
		POWERUPS.NETZACH:
			if $Powerups/Netzach.is_on_cooldown == false:
				$Powerups/Netzach.powerup_effect()
		POWERUPS.BINAH:
			if $Powerups/Binah.is_on_cooldown == false:
				$Powerups/Binah.powerup_effect()
		POWERUPS.HOD:
			if $Powerups/Hod.is_on_cooldown == false:
				$Powerups/Hod.powerup_effect()
		POWERUPS.GEBURAH:
			if $Powerups/Geburah.is_on_cooldown == false:
				$Powerups/Geburah.powerup_effect()
		POWERUPS.CHESED:
			if $Powerups/Chesed.is_on_cooldown == false:
				$Powerups/Chesed.powerup_effect()
		POWERUPS.CHOKMAH:
			if $Powerups/Chokmah.is_on_cooldown == false:
				$Powerups/Chokmah.powerup_effect()


func get_hit_by_enemy():
	match shielded:
		true:
			print("protegido")
		false:
			life -= 1
			UI.update_life()
	if life <= 0:
		game_over()
	


func game_over():
	get_tree().change_scene("res://Scenes/UI Windows/EndGame.tscn")


func next_powerup():
	powerup_index += 1
	if powerup_index > 5:
		powerup_index = 0
	assign_current_powerup()


func previous_powerup():
	powerup_index -= 1
	if powerup_index < 0:
		powerup_index = 5
	assign_current_powerup()


func assign_current_powerup():
	match powerup_index:
		0:
			current_powerup = POWERUPS.BINAH
		1:
			current_powerup = POWERUPS.CHESED
		2: 
			current_powerup = POWERUPS.GEBURAH
		3: 
			current_powerup = POWERUPS.HOD
		4:
			current_powerup = POWERUPS.NETZACH
		5:
			current_powerup = POWERUPS.CHOKMAH
