extends Area2D

var SPEED: int
var direction = Vector2(0,0)
onready var EnemyShotVN = $EnemyShotVisibilityNotifier
var is_being_shot
var screen_entered


func _process(delta: float) -> void:
	position += (direction * SPEED) * delta



func _on_EnemyShot_body_entered(body: Node) -> void:
	if body.has_method("get_hit_by_enemy"):
		body.get_hit_by_enemy()
	get_parent().bullet_destroy(self)



func _on_EnemyShot_area_entered(area: Area2D) -> void:
	if area.has_method("get_hit_by_enemy"):
		area.get_hit_by_enemy()
		get_parent().bullet_destroy(self)
	
