extends Control

func _on_VoltarButton_pressed() -> void:
	queue_free()


func _on_PlayButton_pressed() -> void:
	ScoreManager.setup_new_game_score()
	get_tree().change_scene("res://Scenes/Stage.tscn")
	queue_free()


func _on_IntroNarrative_pressed() -> void:
	var intro_narrative = load("res://Scenes/IntroNarrative.tscn")
	intro_narrative = intro_narrative.instance()
	get_parent().add_child(intro_narrative)


func _on_Parte1Narrativa_pressed() -> void:
	var narrativa1 = load("res://Scenes/Narrative1.tscn")
	narrativa1 = narrativa1.instance()
	get_parent().add_child(narrativa1)


func _on_Parte2Narratova_pressed() -> void:
	var narrativa2 = load("res://Scenes/Narrative2.tscn")
	narrativa2 = narrativa2.instance()
	get_parent().add_child(narrativa2)


func _on_Parte3Narrativa_pressed() -> void:
	var narrativa3 = load("res://Scenes/Narrative3.tscn")
	narrativa3 = narrativa3.instance()
	get_parent().add_child(narrativa3)
