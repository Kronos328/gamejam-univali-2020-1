extends Node

var score = 0
var shots_fired
var shots_hit
var accuracy_rating
var UI
var current_wave
var on_intermission
var enemy_limit = 15
enum MAX_ENEMIES_PER_WAVE {
	WAVE1 = 2, 
	WAVE2 = 3,
	WAVE3 = 4,
	WAVE4 = 5,
	WAVE5 = 6,
	WAVE6 = 7,
	WAVE8 = 8,
	}
enum WAVEZ {
	WAVE1, 
	WAVE2,
	WAVE3,
	WAVE4,
	WAVE5,
	WAVE6,
	WAVE8,
	}
var enemies_on_level = 0



func _ready() -> void:
	
	SilentWolf.configure({
	"api_key": "cQ02bo9MLEZHLwMs8yPP5483hKJMYMj40EhpaKCg", 
	"game_id": "TheLastGear",
	"game_version": "1.0.2",
	"log_level": 1 
	})	
	SilentWolf.configure_scores({"open_scene_on_close": "res://Scenes/MainMenu.tscn"})	
	setup_new_game_score()


func add_score(value):
	score += value


func shot_fired():
	shots_fired += 1


func shot_hit():
	shots_hit += 1


func setup_new_game_score():
	shots_fired = 0
	shots_hit = 0
	accuracy_rating = 1
	score = 0
	enemies_on_level = 0

func update_score_call():
	UI.update_score(score)

func start_wave_intermission(current_wave):
	match current_wave:
		WAVEZ.WAVE1:
			pass
		WAVEZ.WAVE2:
			pass
		WAVEZ.WAVE3:
			pass
		WAVEZ.WAVE4:
			pass
		WAVEZ.WAVE5:
			pass
		WAVEZ.WAVE6:
			pass
		WAVEZ.WAVE7:
			pass
