extends Node2D

var bullet_array = []
var bullet_array_index = 0
export (int) var bullet_speed
export (int) var max_bullets
export (int) var bullets_per_frame

func _ready():
	var bullet = preload("res://Objects/EnemyShot.tscn")
	for num in range(max_bullets):
		var bullet_instance = bullet.instance()
		bullet_instance.SPEED = 0
		bullet_instance.position = position
		bullet_instance.is_being_shot = false
		add_child(bullet_instance)
	bullet_array = get_children()



func _process(delta: float) -> void:
			for bullet in bullet_array:
				if bullet.is_being_shot:
					bullet.position += (bullet.direction * bullet.SPEED) * delta
				if bullet.EnemyShotVN.is_on_screen():
					bullet.screen_entered = true
				elif !bullet.EnemyShotVN.is_on_screen() and bullet.screen_entered:
					bullet_destroy(bullet)

func grab_bullet(shoot_location):
	bullet_array[bullet_array_index].global_position = shoot_location.global_position
	bullet_array[bullet_array_index].direction = shoot_location.position
	bullet_array[bullet_array_index].direction = bullet_array[bullet_array_index].direction.normalized()
	bullet_array[bullet_array_index].SPEED = bullet_speed
	bullet_array[bullet_array_index].is_being_shot = true
	bullet_array_index_increment()
	pass


func bullet_array_index_increment():
	bullet_array_index += 1 
	if bullet_array_index >= max_bullets:
		bullet_array_index = 0


func bullet_destroy(bullet):
	bullet.SPEED = 0
	bullet.global_position = global_position
	bullet.is_being_shot = false
	bullet.screen_entered = false

