# PT-BR

Link para o jogo pode ser encontrado [aqui](https://kronos328.itch.io/the-last-gear)

## Introdução:

In The Last Gear, você controla uma nave consciente que está em busca de respostas acerca da destruição que devastou o Mundo Material.

Explore as ruínas! Destrua os inimigos! Use os poderes das Sefirot!


## Gênero:

The Last Gear é um Shooter Arcade inspirado nos clássicos jogos de naves de fliperama. Porém, com um interação forte com conceitos misticos, metafísicos e ontológicos.

## Tema:

The Last Gear foi feito em cima da ideia de Pos Apocalipse, com uma abordagem mais mística em cima de conceitos metafísicos inspirados dos conceitos de Cabalá e Sefirots. O jogo conta a história de uma nave auto-consciente que busca compreender o que destruiu o Mundo Material (Físico), juntando as Sefirot para chegar ao Infinito e poder impedir essa destruição. Em jogo, isso é representado com o jogador em controle de uma nave em busca das Sefirots e que tem que sobreviver enquanto Hordas de inimigos, protetores das Sefirot irão tentar destruí-lo. Após passar pela sexta Wave o jogador alcança o Infinito e a partida só acaba quando as hordas de fato destruí-lo.


## Controles:

Movimento (WASD)

Mira (Mouse)

Atirar (Botão Esquerdo do Mouse)

Selecionar Power Up Anterior (Q ou Mouse Scroll para Cima)

Selecionar Próximo Power Up (E ou Mouse Scroll para Baixo)

Usar Power Up (Botão Direito do Mouse)


## Desenvolvido por:

Ian Duarte de Aguiar (Game Designer | Programmer)

João Vitor Oliveira Barbosa (Game Designer | Narrative Designer | UI / UX Designer)

Pedro Henrique Macial Vahl (General Artist)


# EN

Link to a playable build can be found [here](https://kronos328.itch.io/the-last-gear)

## Introduction:

In The Last Gear, you take control of a self-conscious ship and search for answers about the destruction that devastated the Physical World.

Explore the ruins! Destroy Enemies! Use the power of the Sefirot!


## Genre:

The Last Gear is an Arcade Shooter inspired by the classic arcade spaceship games. However, with a strong interaction with mystical, metaphysical and ontological concepts.

## Theme:

The Last Gear was made based on the idea of Post Apocalypse, with a more mystical approach based on metaphysical concepts inspired by the concepts of Kabbalah and Sefirots.

The game tells the story of a self-conscious ship that seeks to understand what destroyed the physical world, joining the Sephiroths to reach the Infinite and to be able to prevent this destruction.


## Controls:

Movement (WASD)

Aim (Move Mouse)

Shoot (Left Click Mouse)

Change to Previous Power Up (Q key or Mouse Scroll Up)

Change to Next Power Up (E key or Mouse Scroll Down)

## Developed by

Ian Duarte de Aguiar (Game Designer | Programmer)

João Vitor Oliveira Barbosa (Game Designer | Narrative Designer | UI / UX Designer)

Pedro Henrique Macial Vahl (General Artist)
